/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package regio_vinco;

import java.util.TimerTask;
import javafx.application.Platform;
import javafx.scene.text.Text;

/**
 *
 * @author Kimberly Allan
 */
public class GameTimerTask extends TimerTask {
    private long secs = 0;
    RegioVincoDataModel data;
    
    public GameTimerTask(RegioVincoDataModel data) {
        this.data = data;
    }
    
    @Override
    public void run() {
        secs++;
        Platform.runLater(() -> {
            data.setTimeText(data.getSecondsAsTimeText(secs));
        });   
    }
    
    public long getTimeInSeconds() {
        return secs;
    }

}
