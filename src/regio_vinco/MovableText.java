package regio_vinco;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import static regio_vinco.RegioVinco.SUB_REGION_FILE_PATH;

/**
 * This class represents a game text node that can be moved and
 * rendered and can respond to mouse interactions.
 * @author Richard McKenna and Kimberly Allan
 */
public class MovableText {
    // A JAVAFX TEXT NODE IN THE SCENE GRAPH
    protected Text text;
    
    //Box for text
    protected ImageView box;
    
    // USED FOR MANAGING NODE MOVEMENT
    protected double[] velocity = new double[2];
    protected double[] acceleration = new double[2];

    /**
     * Constructor for initializing a GameNode, note that the provided
     * text argument should not be null.
     * 
     * @param initText The text managed by this object.
     */
    public MovableText(Text initText) {
	text = initText;
        box = new ImageView(new Image("file:" + SUB_REGION_FILE_PATH));
        box.setVisible(false);
    }
    
    // ACCESSOR AND MUTATOR METHODS

    public void boxVisibility(boolean visible) {
        box.setVisible(visible);
    }
    
    public void setBoxColor(Color c) {
        Image b = box.getImage();
        WritableImage img = new WritableImage((int)b.getWidth(), (int)b.getHeight());
        PixelWriter writer = img.getPixelWriter();
        Color cb = c.brighter();
        Color cd = c.darker();
        
        //create image in different color
        for (int x = 0; x < (int)b.getWidth(); x++) {
            for (int y = 0; y < (int)b.getHeight(); y++) {
                writer.setColor(x, y, c);
            }
        }
        //create beveled border 2 px wide
        for (int x = 0; x < (int)b.getWidth(); x++) {
            for (int y = 0; y < 2; y++) {
                writer.setColor(x, y, cb);
            }
            for (int y = (int)b.getHeight() - 1; y > (int)b.getHeight() - 3; y--) {
                writer.setColor(x, y, cd);
            }
        }
        for (int y = 0; y < (int)b.getHeight(); y++) {
            for (int x = 0; x < 2; x++) {
                writer.setColor(x, y, cb);
            }
            for (int x = (int)b.getWidth() - 1; x > (int)b.getWidth() - 3; x--) {
                writer.setColor(x, y, cd);
            }
        }
        
        box.setImage(img);
    }
    
    public ImageView getBox() {
        return box;
    }
    
    public Text getText() {
	return text;
    }
    
    public void setText(Text initText) {
	text = initText;
    }
    
    public double getVelocityX() {
	return velocity[0];
    }
    
    public double getVelocityY() {
	return velocity[1];
    }
    
    public void setVelocityX(double initVelocityX) {
	velocity[0] = initVelocityX;
    }
    
    public void setVelocityY(double initVelocityY) {
	velocity[1] = initVelocityY;
    }

    /**
     * Called each frame, this function moves the node according
     * to its current velocity and updates the velocity according to
     * its current acceleration, applying percentage as a weighting for how
     * much to scale the velocity and acceleration this frame.
     * 
     * @param percentage The percentage of a frame this the time step
     * that called this method represents.
     */
    public void update(double percentage) {
	// UPDATE POSITION
	//@Kimberly Allan
        double xT = text.translateXProperty().doubleValue();
	text.translateXProperty().setValue(xT + (velocity[0] * percentage));
        //move rectangle x value
        double xB = box.translateXProperty().doubleValue();
        box.translateXProperty().setValue(xB + (velocity[0] * percentage));
	double yT = text.translateYProperty().doubleValue();
	//fixed bug- x to y
        text.translateYProperty().setValue(yT + (velocity[1] * percentage));
        //move rectangle y value
        double yB = box.translateYProperty().doubleValue();
        box.translateYProperty().setValue(yB + (velocity[1] * percentage));
        
	
	// UPDATE VELOCITY
	velocity[0] += (acceleration[0] * percentage);
	velocity[1] += (acceleration[1] * percentage);
    }
}